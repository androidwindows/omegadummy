//
//  AppDelegate.h
//  OmegaDummy
//
//  Created by Czarteza Sombilon on 6/13/18.
//  Copyright © 2018 Czarteza Sombilon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

