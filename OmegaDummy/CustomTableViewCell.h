//
//  CustomTableViewCell.h
//  OmegaDummy
//
//  Created by Orlydon Bautista on 13/06/2018.
//  Copyright © 2018 Czarteza Sombilon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *myLabel;

@property (weak, nonatomic) IBOutlet UIImageView *cellimage;

@property (weak, nonatomic) IBOutlet UIImageView *imageContainer;


@end
