//
//  CustomTableViewCell.m
//  OmegaDummy
//
//  Created by Orlydon Bautista on 13/06/2018.
//  Copyright © 2018 Czarteza Sombilon. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setMyLabelText : (NSString *) data{
    self.myLabel.text = data;
    
}

@end
