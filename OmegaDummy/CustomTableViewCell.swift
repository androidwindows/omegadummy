//
//  CustomTableViewCell.swift
//  OmegaDummy
//
//  Created by Orlydon Bautista on 13/06/2018.
//  Copyright © 2018 Czarteza Sombilon. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
