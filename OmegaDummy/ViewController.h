//
//  ViewController.h
//  OmegaDummy
//
//  Created by Czarteza Sombilon on 6/13/18.
//  Copyright © 2018 Czarteza Sombilon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"
@interface ViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *dummydata;
}
@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@property (assign, nonatomic) IBOutlet CustomTableViewCell *cell;


@end

