//
//  ViewController.m
//  OmegaDummy
//
//  Created by Czarteza Sombilon on 6/13/18.
//  Copyright © 2018 Czarteza Sombilon. All rights reserved.
//

#import "ViewController.h"
#import "CustomTableViewCell.h"
typedef void (^CompletionBlock)(UIImage*);
@interface imageLoader:NSObject
- (void)loadImage:(NSString *) url withcompletionblock:(CompletionBlock)completionBlock;
@end
@implementation imageLoader

- (void)loadImage:(NSString *) url withcompletionblock:(CompletionBlock)completionBlock {
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
    if (image == nil) {
        image = [UIImage imageNamed:@"parkshinhye"];
    }
    //NSLog(@"Action Performed");
    completionBlock(image);
}

@end

@interface ViewController ()


@end

@implementation ViewController


@synthesize cell = _cell;
- (void)viewDidLoad {
    [super viewDidLoad];

    [self.myTableView registerNib:[UINib nibWithNibName:@"CustomTableViewCell" bundle:nil]
           forCellReuseIdentifier:@"CustomTableViewCell"];
    dummydata = [[NSMutableArray alloc] init];
    [dummydata addObject:@{
                           @"text" : @"sda sdasd asda sda sdasd asda dasda sda dsa",
                           @"url" : @"https://www.thefamouspeople.com/profiles/images/kim-so-hyun-1.jpg"
                           }];
    [dummydata addObject:@{
                           @"text" : @"sdasdqweqweasdasdasdasdafa asd asasd asd asda fasd asd",
                           @"url" : @"https://www.thefamouspeople.com/profiles/images/kim-so-hyun-1.jpg"
                           }];
    [dummydata addObject:@{
                           @"text" : @"iooirtjhorthjofijgoriutoirj oritj roitjy oirtjy iortjyorijty",
                           @"url" : @"https://www.thefamouspeople.com/profiles/images/kim-so-hyun-1.jpg"
                           }];
    [dummydata addObject:@{
                           @"text" : @"nbvxmcnvbmxcbvmnsdbms bmnsdfb msdnbf nsbdfnbxcmvnb xmcnbv",
                           @"url" : @"https://www.thefamouspeople.com/profiles/images/kim-so-hyun-1.jpg"
                           }];
    [dummydata addObject:@{
                           @"text" : @"poipopkjgfhlkfjghkfjgkhj lrktjylk rjtylkrjtiohrt oijroghkjr oitj",
                           @"url" : @"https://www.thefamouspeople.com/profiles/images/kim-so-hyun-1.jpg"
                           }];
    [dummydata addObject:@{
                           @"text" : @"7652346725374652763452765 467253 724637645 236745276354725 346725 7246725372 376452736",
                           @"url" : @"https://www.thefamouspeople.com/profiles/images/kim-so-hyun-1.jpg"
                           }];
    [dummydata addObject:@{
                           @"text" : @"zxczxczxcz xczxc zxc zxczx czxcz xcz xczx czxc zxc zxcz xczx czxc zxc zxc zxcz xcz xcz xcz xczx czxczxczxczxczxczxczxczxczxczxczxczxczxc",
                           @"url" : @"https://www.thefamouspeople.com/profiles/images/kim-so-hyun-1.jpg"
                           }];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    // Do any additional setup after loading the view, typically from a nib.
    for (int i=0; i<=4; i++) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        NSString *name = [NSString stringWithFormat:@"Name%d",i];
        UIImage *image = [UIImage imageNamed:@"parkshinhye"];
        [dict setObject: name forKey:@"name"];
        [dict setObject:image forKey:@"image"];
        [dummydata addObject: dict ];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CustomTableViewCell *cell = [_myTableView dequeueReusableCellWithIdentifier:@"CustomTableViewCell"];
    NSDictionary *dict;
    dict = dummydata[indexPath.row];
    cell.myLabel.text =  [dict valueForKey:@"name"];
    cell.cellimage.image = [dict objectForKey:@"image"];
//    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *urlString = [dummydata[indexPath.row] objectForKey:@"url"];
//        cell.imageContainer.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
//    });
    imageLoader *loader = [[imageLoader alloc]init];
    [loader loadImage:urlString withcompletionblock:^(UIImage *image){
        
        cell.imageContainer.contentMode = UIViewContentModeScaleToFill;
        cell.imageContainer.image = image;
    }];
    
    
    
    return cell;
}


- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dummydata.count;
}

-(NSMutableArray *) tableData{
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    for (int i=0; i<=9; i++) {
        
        NSString *data =  @"test";
        
        [array addObject: data];
        
        
        
    }
    
    return array;
    
}












@end
