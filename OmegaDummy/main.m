//
//  main.m
//  OmegaDummy
//
//  Created by Czarteza Sombilon on 6/13/18.
//  Copyright © 2018 Czarteza Sombilon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
